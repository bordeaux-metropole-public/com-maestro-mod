<?php

namespace Drupal\bm_maestro\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Url;
use Drupal\maestro\Controller\MaestroOrchestrator;
use Drupal\maestro\Engine\MaestroEngine;
use Drupal\maestro\Utility\TaskHandler;
use Drupal\maestro_taskconsole\Controller\MaestroTaskConsoleController;

class NewMaestroTaskConsoleController extends MaestroTaskConsoleController {

  /**
   * @inheritDoc
   */
  public function getTasks($highlight_queue_id = 0) {
    global $base_url;

    $config = \Drupal::config('maestro.settings');
    // Before we do anything, let's see if we should be running the orchestrator through task console refreshes:
    if ($config->get('maestro_orchestrator_task_console')) {
      $orchestrator = new MaestroOrchestrator();
      $orchestrator->orchestrate($config->get('maestro_orchestrator_token'));
    }
    $engine = new MaestroEngine();

    $task_console_table = [
      '#type' => 'table',
      '#header' => [
        $this->t('Task'),
        $this->t('Flow'),
        $this->t('Assigned'),
        $this->t('Actions'),
        $this->t('Details'),
      ],
      '#empty' => $this->t('You have no tasks.'),
      '#attributes' => [
        'class' => ['taskconsole-tasks'],
      ],
    ];

    // Fetch the user's queue items.
    $current_user_id = \Drupal::currentUser()->id();
    $queue_ids = MaestroEngine::getAssignedTaskQueueIds($current_user_id);

    foreach ($queue_ids as $queue_id) {
      $highlight_class = '';
      $url_from_route = FALSE;
      if ($highlight_queue_id == $queue_id) {
        // Set the highlight for the queue entry.
        $highlight_class = 'maestro-highlight-task';
      }

      /*
       *  Reset the internal static cache for this queue record and then reload it
       *  Doing this because we found in certain cases it was not reflecting actual queue record
       */
      $queue_storage = \Drupal::entityTypeManager()->getStorage('maestro_queue');
      $queue_storage->resetCache([$queue_id]);
      /** @var $queue_record \Drupal\maestro\Entity\MaestroQueue */
      $queue_record = $queue_storage->load($queue_id);

      $process_id = MaestroEngine::getProcessIdFromQueueId($queue_id);
      /** @var $process_record \Drupal\maestro\Entity\MaestroProcess */
      $process_record = MaestroEngine::getProcessEntryById($process_id);
      $queue_token = MaestroEngine::getTokenFromQueueId($queue_id);

      $queue_row = [];

      $queue_row['#attributes'] = ['class' => $highlight_class];

      $queue_row['task'] = [
        '#plain_text' => $this->t($queue_record->task_label->getString()),
      ];

      $queue_row['flow'] = [
        '#plain_text' => $this->t($process_record->process_name->getString()),
      ];

      $queue_row['assigned'] = [
        '#plain_text' => \Drupal::service('date.formatter')->format(
          $queue_record->created->getString(), 'custom', 'd/m/Y H:i'),
      ];

      $template_machine_name = $engine->getTemplateIdFromProcessId(
        $queue_record->process_id->getString());
      $task_template = $engine->getTemplateTaskByID(
        $template_machine_name, $queue_record->task_id->getString());
      $template = MaestroEngine::getTemplate($template_machine_name);

      // Default link title.
      $link_title = 'Execute';
      $use_modal = FALSE;
      $sitewide_token = $config->get('maestro_sitewide_token');
      if (!empty($queue_token) && $sitewide_token != '') {
        $query_options = ['queueid_or_token' => $queue_token];
      }
      else {
        $query_options = ['queueid_or_token' => $queue_id];
      }

      if (isset($task_template['data']['modal'])
        && $task_template['data']['modal'] == 'modal') {
        $use_modal = TRUE;
      }
      /*
       * If this is an interactive Maestro task, it means we show an Operations Dropbutton form element
       * This is a  button with one or more links where the links can be to a node add/edit or
       * to open up a modal window for an interactive task like a form approval action.
       *
       * We need to determine if we have any special handling for this interactive task. It could be
       * a link to an external system.
       */

      /*
       * Test to see if this is a URL that can be deduced from a Drupal route or not.
       * if it's not a route, then $url_from_route will be FALSE
       */

      $handler_url = $queue_record->handler->getString();
      if ($queue_record->is_interactive->getString() == '1') {
        if (empty($handler_url)) {
          // Handler is empty.  If this is an interactive task and has no handler, we're still OK.
          // This is an interactive function that uses a default handler then.
          $handler_type = 'function';
        }
        else {
          $handler_url = str_replace($base_url, '', $handler_url);
          $handler_type = TaskHandler::getType($handler_url);

          $handler_url_parts = UrlHelper::parse($handler_url);
          $query_options += $handler_url_parts['query'];

          // Let's call a hook here to let people change the name of the link to execute the task if they so choose to do so.
          \Drupal::moduleHandler()
            ->invokeAll('maestro_task_console_interactive_link_alter',
              [
                &$link_title,
                $task_template,
                $queue_record,
                $template_machine_name,
              ]
            );
        }
      }
      else {
        // We shouldn't be processing this. Skip the rest.
        continue;
      }

      $operations_links = [];

      switch ($handler_type) {
        case 'external':
          $operations_links['maestro_link'] = [
            'title' => $this->t($link_title),
            'url' => Url::fromUri($handler_url, ['query' => $query_options]),
          ];
          break;

        case 'internal':
          // Let's call a hook here to let people change the name of the link to execute the task if they so choose to do so.
          \Drupal::moduleHandler()
            ->invokeAll('maestro_task_console_interactive_link_alter',
              [
                &$link_title,
                $task_template,
                $queue_record,
                $template_machine_name,
              ]
            );

          // We don't want Moestro hooks to mess with $query_options.
          $query_options_copy = [...$query_options];
          // Let's call a hook here to let people change the actual link.
          \Drupal::moduleHandler()
            ->invokeAll('maestro_task_console_interactive_url_alter',
              [
                &$handler_url,
                &$query_options_copy,
                $task_template,
                $queue_record,
                $template_machine_name,
                'internal',
              ]
            );

          $operations_links['maestro_link'] = [
            'title' => $this->t($link_title),
            'url' => Url::fromUserInput($handler_url, ['query' => $query_options]),
          ];
          break;

        case 'function':
          // Let's call a hook here to let people change the name of the link to execute the task if they so choose to do so.
          \Drupal::moduleHandler()
            ->invokeAll('maestro_task_console_interactive_link_alter',
              [
                &$link_title,
                $task_template,
                $queue_record,
                $template_machine_name,
              ]
            );

          // We don't want Moestro hooks to mess with $query_options.
          $query_options_copy = [...$query_options];
          // Let's call a hook here to let people change the actual link.
          \Drupal::moduleHandler()
            ->invokeAll('maestro_task_console_interactive_url_alter',
              [
                &$handler_url,
                &$query_options_copy,
                $task_template,
                $queue_record,
                $template_machine_name,
                'function',
              ]
            );

          if ($use_modal) {
            $query_options += ['modal' => 'modal'];
            $operations_links[$link_title] = [
              'title' => $this->t($link_title),
              'url' => Url::fromRoute('maestro.execute', $query_options),
              'attributes' => [
                'class' => ['use-ajax'],
                'data-dialog-type' => 'modal',
                'data-dialog-options' => Json::encode([
                  'width' => 700,
                ]),
              ],
            ];
          }
          else {
            $query_options += ['modal' => 'notmodal'];
            $operations_links[$link_title] = [
              'title' => $this->t($link_title),
              'url' => Url::fromRoute('maestro.execute', $query_options),
            ];
          }
          break;

        default:
      }

      // Let's call a hook here to let people change the actual link.
      \Drupal::moduleHandler()
        ->invokeAll('maestro_task_console_operations_alter',
          [
            &$operations_links,
            $query_options,
            $task_template,
            $queue_record,
            $template_machine_name,
          ]
        );

      if (count($operations_links) > 0) {
        $operations = [
          'data' => [
            '#type' => 'operations',
            '#links' => $operations_links,
          ],
        ];
      }
      else {
        $operations = [
          '#plain_text' => $this->t('Invalid Link'),
        ];
      }

      $queue_row['operations'] = $operations;

      /*
       * Provide your own execution links here if you wish
       */
      \Drupal::moduleHandler()
        ->invokeAll('maestro_task_console_alter_execution_link',
          [
            &$queue_row['operations'],
            $task_template,
            $queue_record,
            $template_machine_name,
          ]
        );

      $queue_row['expand'] = [
        '#wrapper_attributes' => ['class' => ['maestro-expand-wrapper']],
        '#plain_text' => '',
      ];

      $var_workflow_stage_count = intval(MaestroEngine::getProcessVariable(
        'workflow_timeline_stage_count', $process_id));

      // If the show details is on OR the status bar is on, we'll show the toggler.
      $has_details = ((isset($template->show_details) && $template->show_details) ||
        (isset($template->default_workflow_timeline_stage_count)
          && intval($template->default_workflow_timeline_stage_count) > 0
          && $var_workflow_stage_count > 0));

      if ($has_details) {
        // Provide details expansion column.  Clicking on it will show the status and/or the task detail information via ajax.
        $queue_row['expand'] = [
          '#wrapper_attributes' => [
            'class' => [
              'maestro-expand-wrapper',
              'maestro-status-toggle-' . $queue_id,
            ],
          ],
          '#attributes' => [
            'class' => ['maestro-timeline-status', 'maestro-status-toggle'],
            'title' => $this->t('Open Details'),
          ],
          '#type' => 'link',
          '#id' => 'maestro-id-ajax-' . $queue_id,
          '#url' => Url::fromRoute('maestro_taskconsole.status_ajax_open', [
            'processID' => $process_id,
            'queueID' => $queue_id,
          ]),
          '#title' => $this->t('Open Details'),
          '#ajax' => [
            'progress' => [
              'type' => 'throbber',
              'message' => NULL,
            ],
          ],
        ];
      }

      $task_console_table[$queue_id] = $queue_row;

      if ($has_details) {
        // Gives the <tr> tag an ID we can target.
        $task_console_table[$queue_id . '_ajax']['#attributes']['id'] = $queue_id . '_ajax';
        $task_console_table[$queue_id . '_ajax']['#attributes']['class'] = ['maestro-ajax-row'];
        $task_console_table[$queue_id . '_ajax']['task'] = [
          '#wrapper_attributes' => ['colspan' => count($queue_row) - 1],
          '#prefix' => '<div id="maestro-ajax-' . $queue_id . '">',
          '#suffix' => '</div>',
        ];
      }
    }

    $build = [];

    $build['task_console_table'] = $task_console_table;

    $build['#attached']['library'][] = 'maestro_taskconsole/maestro_taskconsole_css';
    // Css for the status bar.
    $build['#attached']['library'][] = 'maestro/maestro-engine-css';
    $build['#attached']['drupalSettings'] = [
      'baseURL' => base_path(),
    ];

    return $build;
  }

}
