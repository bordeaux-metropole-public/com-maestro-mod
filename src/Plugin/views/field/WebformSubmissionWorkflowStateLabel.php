<?php

namespace Drupal\bm_maestro\Plugin\views\field;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\views\ViewExecutable;

/**
 * Field handler to display the workflow state for a webform submission.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("webform_submission_workflow_state_label")
 */
class WebformSubmissionWorkflowStateLabel extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    // D'après le code présent dans la classe StatisticsLastCommentName.
    $workflow_element_name =
      $this->options['workflow_element_name'] ?: 'workflow';
    $definition = [
      'table' => 'webform_submission_data',
      'field' => 'sid',
      'left_table' => 'webform_submission',
      'left_field' => 'sid',
      'extra' => [
        ['field' => 'name', 'value' => $workflow_element_name ],
        ['field' => 'property', 'value' => 'workflow_state_label'],
        ['field' => 'delta', 'value' => 0]
      ],
    ];
    $join = \Drupal::service('plugin.manager.views.join')->createInstance('standard', $definition);
    $wsd_table = $this->query->ensureTable($this->getPluginId(), $this->relationship, $join);
    $this->field_alias = $this->query->addField($wsd_table, 'value');
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['workflow_element_name'] = ['default' => 'workflow'];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);
    $form['workflow_element_name'] = [
      '#title' => $this->t('Workflow element name'),
      '#type' => 'textfield',
      '#default_value' => $this->options['workflow_element_name'] ?: 'workflow',
    ];
  }

}
