<?php

namespace Drupal\bm_maestro\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Controller\ControllerResolverInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block that displays the Maestro task console.
 *
 * @Block(
 *   id = "maestro_task_console",
 *   admin_label = @Translation("Maestro task console"),
 *   category = @Translation("Maestro"),
 * )
 */
class MaestroTaskConsoleBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The controller resolver service.
   *
   * @var \Drupal\Core\Controller\ControllerResolverInterface
   */
  protected $controllerResolver;

  /**
   * Constructs a new \Drupal\mymodule\Plugin\Block\MymoduleBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Controller\ControllerResolverInterface $controller_resolver
   *   The controller resolver service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ControllerResolverInterface $controller_resolver) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->controllerResolver = $controller_resolver;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('controller_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $controller_definition = '\Drupal\bm_maestro\Controller\NewMaestroTaskConsoleController::getTasks';
    $controller = $this->controllerResolver->getControllerFromDefinition($controller_definition);
    return call_user_func($controller);
  }

  /**
   * {@inheritDoc}
   *
   * @see \Drupal\Core\Plugin\ContextAwarePluginBase::getCacheMaxAge()
   */
  public function getCacheMaxAge() {
    // Caching turned off for this block.
    return 0;
  }

}
