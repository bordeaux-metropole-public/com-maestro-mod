<?php

namespace Drupal\bm_maestro\Form;

use Drupal\ajax_command_page_reload\Ajax\PageReloadCommand;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\maestro\Engine\MaestroEngine;

/**
 * The Maestro Queue Reassign form.
 */
class MaestroQueueReassign extends FormBase {

  use AjaxFormHelperTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'maestro_queue_reassignment_form';
  }

  /**
   * This is the reassignment form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $queueid_or_token = NULL) {

    if (intval($queueid_or_token) == $queueid_or_token) {
      $queue_id = $queueid_or_token;
    }
    else {
      //need to fetch off the queueID based on token
      $queue_id = MaestroEngine::getQueueIdFromToken($queueid_or_token);
    }

    $queue_entity = MaestroEngine::getQueueEntryById($queue_id);

    if ($queue_entity) {

      // Add referer if not an Ajax request.
      if (!$this->isAjax()) {
        $form['referer'] = [
          '#type' => 'hidden',
          '#default_value' => \Drupal::request()->server->get('HTTP_REFERER'),
        ];
      }

      $form['queue_id'] = [
        '#type' => 'hidden',
        '#default_value' => $queue_id,
      ];

      $form['keep_existing_assignments'] = [
        '#type' => 'hidden',
        '#default_value' => '',
      ];

      $form['assign_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Assign to'),
        '#options' => [
          'myself' => $this->t('Myself'),
          'user' => $this->t('User'),
          'role' => $this->t('Role'),
        ],
        '#default_value' => 'myself',
      ];

      // Provide a user lookup.
      $form['select_assigned_user'] = [
        '#id' => 'select_assigned_user',
        '#type' => 'entity_autocomplete',
        '#target_type' => 'user',
        '#default_value' => '',
        '#selection_settings' => ['include_anonymous' => FALSE],
        '#title' => $this->t('User to Reassign To'),
        '#required' => FALSE,
        '#wrapper_attributes' => [ // Hide by default.
          'style' => ['display: none;']
        ],
        '#states' => [
          'visible' => [
            ':input[name="assign_type"]' => ['value' => 'user'],
          ],
        ],
      ];

      // Provide a role lookup.
      $form['select_assigned_role'] = [
        '#id' => 'select_assigned_role',
        '#type' => 'entity_autocomplete',
        '#target_type' => 'user_role',
        '#default_value' => '',
        '#title' => $this->t('Role to Reassign To'),
        '#required' => FALSE,
        '#wrapper_attributes' => [ // Hide by default.
          'style' => ['display: none;']
        ],
        '#states' => [
          'visible' => [
            ':input[name="assign_type"]' => ['value' => 'role'],
          ],
        ],
      ];

      $form['actions'] = [
        '#type' => 'actions',
      ];

      if ($this->isAjax()) {
        $form['actions']['submit'] = [
          '#type' => 'button',
          '#value' => $this->t('Do Reassignment'),
          '#ajax' => [
            'callback' => '::ajaxSubmit',
          ],
        ];
      }
      else {
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Do Reassignment'),
        ];
        $form['actions']['cancel'] = [
          '#type' => 'submit',
          '#value' => $this->t('Cancel'),
        ];
      }


      return $form;
    }
    // This queue entry doesn't exist.  Stop messing around!
    else {
      \Drupal::messenger()->addError(t('Invalid queue ID!'));
      return ['#markup' => $this->t('Invalid Queue Record. Operation Halted.')];
    }

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (str_contains($triggering_element['#id'], 'edit-submit')) {
      // https://drupal.stackexchange.com/questions/206660/unset-form-error-for-specific-field
      // Temporarily store all form errors.
      $form_errors = $form_state->getErrors();
      // Clear the form errors.
      $form_state->clearErrors();
      // Based on the assignment type, we must validate the form to ensure either the user or role has been entered, and that it's valid.
      // if the assign type is anything but the role or user, we offload to other modules to handle.
      $assign_type = $form_state->getValue('assign_type');
      if ($assign_type == 'myself') {
        unset($form_errors['select_assigned_user']);
        unset($form_errors['select_assigned_role']);
      }
      elseif ($assign_type == 'user') {
        unset($form_errors['select_assigned_role']);
        $user = $form_state->getValue('select_assigned_user');
        if (!isset($user)) {
          $form_state->setErrorByName('select_assigned_user', $this->t('You must choose a user to reassign to'));
        }
      }
      elseif ($assign_type == 'role') {
        unset($form_errors['select_assigned_user']);
        $role = $form_state->getValue('select_assigned_role');
        if (!isset($role)) {
          $form_state->setErrorByName('select_assigned_role', $this->t('You must choose a role to reassign to'));
        }
      }
      // Now loop through and re-apply the remaining form error messages.
      foreach ($form_errors as $name => $error_message) {
        $form_state->setErrorByName($name, $error_message);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxCancel(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    $this->submitForm($form, $form_state);
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    $response->addCommand(new PageReloadCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (str_contains($triggering_element['#id'], 'edit-submit')) {
      // We do the reassignment here.
      $queue_id = intval($form_state->getValue('queue_id'));
      if ($queue_id == 0) {
        \Drupal::logger('bm_maestro')->warning($this->t('Task unavailable.'));
        return;
      }

      $current_user = \Drupal::currentUser();
      // First check current user has been assigned to this queue.
      if (!MaestroEngine::canUserExecuteTask($queue_id, $current_user->id())) {
        \Drupal::logger('bm_maestro')
          ->warning($this->t('Unauthorized access.'));
        return;
      }

      $assign_type = $form_state->getValue('assign_type');
      $assignee = '';
      if ($assign_type == 'myself') {
        $assign_type = 'user';
        $assignee = $current_user->getDisplayName();
      }
      elseif ($assign_type == 'user') {
        $uid = $form_state->getValue('select_assigned_user');
        // This now holds the user ID.  translate that into username.
        $account = User::load($uid);
        $assignee = $account->getDisplayName();
      }
      elseif ($assign_type == 'role') {
        $assignee = $form_state->getValue('select_assigned_role');
      }

      if (!empty($assignee)) {
        $assignment_storage = \Drupal::entityTypeManager()->getStorage(
          'maestro_production_assignments');

        // Lets get the assignments based on this queue ID.
        $assignment_query = $assignment_storage->getQuery();
        $assignment_query->accessCheck(FALSE);
        $assignment_query->condition('queue_id', $queue_id);
        $current_assignment_ids = $assignment_query->execute();

        $values = [
          'queue_id' => $queue_id,
          'assign_type' => $assign_type,
          'by_variable' => 0,
          'assign_id' => $assignee,
          'process_variable' => 0,
          'assign_back_id' => $current_user->id(),
          'task_completed' => 0,
        ];

        $new_assignment = $assignment_storage->create($values);
        $new_assignment->save();

        $keep_existing_assignments =
          $form_state->getValue('keep_existing_assignments');
        if (empty($keep_existing_assignments)) {
          // Let's remove all other previous assignments.
          foreach ($current_assignment_ids as $assignment_id) {
            $assignment_entity = $assignment_storage->load($assignment_id);
            if ($assignment_entity) {
              $assignment_entity->delete();
            }
          }
        }
      }

      \Drupal::logger('bm_maestro')->info('User @user has assigned @assign_type @assignee to task @queue_id',
      ['@user' => $current_user->getDisplayName(), '@assign_type' => $assign_type, '@assignee' => $assignee, '@queue_id' => $queue_id]);

      \Drupal::messenger()->addStatus(
        $this->t('Reassignment successfully completed.'));
    }

    // Redirect to referer only if not an Ajax request.
    if (!$this->isAjax()) {
      $referer = $form_state->getValue('referer');
      if (!empty($referer)) {
        $form_state->setRedirectUrl(Url::fromUri($referer));
      }
    }
  }

}
